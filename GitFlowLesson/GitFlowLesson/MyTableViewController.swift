//
//  MyTableView.swift
//  GitFlowLesson
//
//  Created by mxbb on 5/13/20.
//  Copyright © 2020 mxbb. All rights reserved.
//

import UIKit

class MyTableViewController: UITableViewController {

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)

        cell.textLabel?.text = "\(indexPath.row) row"
        cell.detailTextLabel?.text = "release version"
        return cell
    }
}
