//
//  ViewController.swift
//  GitFlowLesson
//
//  Created by mxbb on 5/13/20.
//  Copyright © 2020 mxbb. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet var activityIndicator: UIActivityIndicatorView?

    @IBAction func startLoading() {
        activityIndicator?.startAnimating()
    }
}

